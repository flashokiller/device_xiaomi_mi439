#
# Copyright (C) 2021 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/crystal_mi439.mk

COMMON_LUNCH_CHOICES := \
    crystal_mi439-user \
    crystal_mi439-userdebug \
    crystal_mi439-eng
